﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject objectToFollow;
    
    public float speed = 2.0f;

    public float cameraDistance;
    public float cameraDistanceExponent;
    private Rigidbody rb;
  
    // Start is called before the first frame update
    void Awake()
    {
        rb = objectToFollow.GetComponent<Rigidbody>();
    }

    public void init() {
        rb = objectToFollow.GetComponent<Rigidbody>();
    } 

    void FixedUpdate () {
        float interpolation = speed * Time.fixedDeltaTime;
        
        Vector3 targetPosition = objectToFollow.transform.position;
        Vector3 direction = GameObject.Find("Main Camera").transform.position - targetPosition;

        this.transform.rotation = Quaternion.Euler(
            objectToFollow.transform.rotation.eulerAngles.x,
            Mathf.MoveTowardsAngle(this.transform.rotation.eulerAngles.y, objectToFollow.transform.rotation.eulerAngles.y, interpolation),
            objectToFollow.transform.rotation.eulerAngles.z
        );

        this.transform.position = targetPosition + direction.normalized * Mathf.Pow(rb.velocity.magnitude, cameraDistanceExponent) * cameraDistance;

    }
}
