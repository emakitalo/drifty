﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class UIManager : MonoBehaviour
{   
    public GameObject gameManager;
    private GameManager gManager;
    public GameObject mainMenu;
    public GameObject pauseMenu;
    public GameObject selectCarMenu;
    public GameObject carButtonTemplate;
    public GameObject selectLevelMenu;
    public GameObject levelButtonTemplate;
    public GameObject gameRuntime;
    public GameObject controls;
    public float buttonOffset;
    private RenderTexture pauseBackground;
    // Start is called before the first frame update
    void Start()
    {
        pauseMenu.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        gManager = gameManager.GetComponent<GameManager>();
        initCarMenu();
        initLevelMenu();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)) {
            pauseGame();
        }
    }

    void initLevelMenu() {
        levelButtonTemplate.GetComponent<Button>().onClick.AddListener(selectLevel);
        for (var i = 0; i < gManager.levels.Count; i++) {
            GameObject levelButton = Instantiate(levelButtonTemplate, selectLevelMenu.transform);
            RectTransform buttonTransform = levelButton.GetComponent<RectTransform>();
            buttonTransform.position = new Vector2(Screen.width * 0.5f, Screen.height - 50f - buttonOffset * i);
            levelButton.GetComponent<SelectButton>().id = i;
            levelButton.transform.GetChild(0).gameObject.GetComponent<TMP_Text>().text = gManager.levels[i].name;
            levelButton.SetActive(true);
        }
    }

    void initCarMenu() {
        carButtonTemplate.GetComponent<Button>().onClick.AddListener(selectCar);
        for (var i = 0; i < gManager.cars.Count; i++) {
            GameObject carButton = Instantiate(carButtonTemplate, selectCarMenu.transform);
            RectTransform buttonTransform = carButton.GetComponent<RectTransform>();
            buttonTransform.position = new Vector2(Screen.width * 0.5f, Screen.height - 50f - buttonOffset * i);
            carButton.GetComponent<SelectButton>().id = i;
            carButton.transform.GetChild(0).gameObject.GetComponent<TMP_Text>().text = gManager.cars[i].name;
            carButton.SetActive(true);
        }
    }

    public void showLevelMenu() {
        selectLevelMenu.SetActive(true);
        mainMenu.SetActive(false);
    }

    public void showCarMenu() {
        selectCarMenu.SetActive(true);
        mainMenu.SetActive(false);
    }

    public void pauseGame() {
        if(gameRuntime.activeSelf) {
            pauseBackground = new RenderTexture( Screen.width, Screen.height, 24);
            pauseMenu.GetComponent<RawImage>().texture = pauseBackground;
            GameObject.Find("Main Camera").GetComponent<Camera>().targetTexture = pauseBackground;
            GameObject.Find("Main Camera").GetComponent<Camera>().Render();
            GameObject.Find("Main Camera").GetComponent<Camera>().targetTexture = null;
            pauseMenu.SetActive(true);
            gameRuntime.SetActive(false);
            gManager.paused = true;
        } else if(pauseMenu.activeSelf) {
            pauseMenu.SetActive(false);
            gameRuntime.SetActive(true);
            gManager.paused = false;
        }
    }

    public void selectLevel() {
        gManager.currentLevel = EventSystem.current.currentSelectedGameObject.GetComponent<SelectButton>().id; 
        selectLevelMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void selectCar() {
        gManager.currentCar = EventSystem.current.currentSelectedGameObject.GetComponent<SelectButton>().id; 
        selectCarMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void quitGame() {
        if(pauseMenu.activeSelf) {
            pauseMenu.SetActive(false);
            controls.SetActive(false);
            mainMenu.SetActive(true);
        }
    }
}
