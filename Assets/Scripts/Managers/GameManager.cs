﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class GameManager : MonoBehaviour
{
    public GameObject uiContainer;
    private UIManager uiManager;
    private GameObject mainMenu;
    public bool paused;
    private TMP_Text timeText;
    private float time;
    public List<GameObject> cars;
    public List<GameObject> levels;
    public GameObject controls;
    public  GameObject camera;
    public GameObject root;
    public int currentCar;
    public int currentLevel;
    private CameraController cameraController;

    // Start is called before the first frame update
    void Start()
    {
        cameraController = camera.GetComponent<CameraController>();
        uiManager = uiContainer.GetComponent<UIManager>();
        mainMenu = uiManager.mainMenu;
        timeText = uiManager.controls.transform.GetChild(1).GetComponent<TMP_Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!paused) {
            time += Time.deltaTime;
            timeText.text = time.ToString("0.00");
        }
    }

    void startGame(int _level, int _car) {
        foreach (Transform child in root.transform) {
            GameObject.Destroy(child.gameObject);
        }

        Instantiate(levels[_level].gameObject, root.transform);
        GameObject car = Instantiate(cars[_car].gameObject, root.transform);

        cameraController.objectToFollow = car;
        cameraController.init();

        mainMenu.SetActive(false);
        controls.SetActive(true);
        root.SetActive(true);
        time = 0.00f;
        paused = false;
    }

    public void init() {
        startGame(currentLevel, currentCar);
    }
}
