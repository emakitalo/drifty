﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float maxWheelRotation;
    public float rotationMultiplier;
    public float drag;
    public float minDrag;
    public GameObject steeringWheel;
    public float speed;
    public float touchFade;
    public int emissionRate;
    public float angularMultiply;
    public float emissionMultiplier;
    public List<GameObject> wheelsFront;
    public List<ParticleSystem> tireSmokes;
    private float wheelRotation;
    private float xValue;
    private Rigidbody rb; 

    // Start is called before the first frame update
    void Start()
    {
        steeringWheel = GameObject.Find("Wheel");
        rb = gameObject.GetComponent<Rigidbody>();
    }

    void Update() {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            // Move the cube if the screen has the finger moving.
            if (touch.phase == TouchPhase.Moved)
            {
                Vector2 pos = touch.position;
                xValue = Mathf.Pow(Utilities.map(touch.position.x, 0.0f, Screen.width, -1.0f, 1.0f), 2.0f) * touchFade;
            }
        } else if (Input.GetMouseButton(0)) {
            Vector2 pos = Input.mousePosition;
            xValue = Utilities.map(pos.x, 0.0f, Screen.width, -1.0f, 1.0f) * touchFade;
        } else {
            xValue = Mathf.Pow(Input.GetAxis("Horizontal"), 2.0f);
        }

        wheelRotation = xValue * maxWheelRotation;
        steeringWheel.transform.rotation = Quaternion.Euler(0.0f, 0.0f, -wheelRotation);
        foreach(GameObject wheel in wheelsFront) {
            Quaternion newRotation = Quaternion.Euler(
                0.0f,
                0.0f,
                wheelRotation
            );       
            wheel.transform.localRotation = newRotation;
        }

        foreach(ParticleSystem smoke in tireSmokes) {
            var emission = smoke.emission;
            emission.rateOverTime = Utilities.map(
                rb.velocity.magnitude + rb.angularVelocity.magnitude * emissionMultiplier,
                0.0f,
                speed * emissionMultiplier,
                emissionRate,
                0.0f
            );
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float angDrag = Utilities.map(Mathf.Abs(wheelRotation), 0.0f, maxWheelRotation, drag, minDrag);
        rb.angularDrag = angDrag;
        rb.AddTorque(transform.up * Utilities.map(
            wheelRotation, 
            -maxWheelRotation, 
            maxWheelRotation, 
            -rotationMultiplier, 
            rotationMultiplier)
        );
        rb.AddForce(transform.forward * speed * angDrag * Time.deltaTime);
    }
}
