﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class AxleInfo {
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor;
    public bool steering;
}
     
public class CarController : MonoBehaviour {
    public List<AxleInfo> axleInfos; 
    public float maxMotorTorque;
    public float maxSteeringAngle;
    public GameObject centerOfMass;
    public float speed;
    public float stiffness;
    private Rigidbody rb;
    public GameObject steeringWheel;
     
    // finds the corresponding visual wheel
    // correctly applies the transform
    void Start() {
        if(centerOfMass != null) {
            rb = gameObject.GetComponent<Rigidbody>();
            rb.centerOfMass = centerOfMass.transform.position;
        }
    }

    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        if (collider.transform.childCount == 0) {
            return;
        }
     
        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }
     
    public void FixedUpdate()
    {

        float motor = maxMotorTorque * (1.0f - Mathf.Abs(Input.GetAxis("Horizontal"))) + speed;
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");
     
        foreach (AxleInfo axleInfo in axleInfos) {
            if (axleInfo.steering) {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor) {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;

                WheelFrictionCurve sFrictionL = axleInfo.leftWheel.sidewaysFriction;
                WheelFrictionCurve sFrictionR = axleInfo.rightWheel.sidewaysFriction;

                float newStifness = Mathf.Abs(Input.GetAxis("Horizontal")) + stiffness;
                sFrictionL.stiffness = Utilities.map(newStifness, 0.0f, 1.0f + stiffness, 1.0f, 0.0f);
                sFrictionR.stiffness = Utilities.map(newStifness, 0.0f, 1.0f + stiffness, 1.0f, 0.0f);

                axleInfo.leftWheel.sidewaysFriction = sFrictionL;
                axleInfo.rightWheel.sidewaysFriction = sFrictionR;
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
        }

        Vector3 rot = steeringWheel.transform.eulerAngles;
        steeringWheel.transform.rotation = Quaternion.Euler(rot.x,rot.y,-steering);
    }
}